package co.com.jose.pilae;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("co.com.jose.pilae.*")
@ComponentScan(basePackages = { "co.com.jose.pilae.*" })
@EntityScan("co.com.jose.pilae.*")
public class PilaeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PilaeApplication.class, args);
	}
}