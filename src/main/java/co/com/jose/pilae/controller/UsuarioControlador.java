package co.com.jose.pilae.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.jose.pilae.dominio.usuario.IUsuarioDominio;
import co.com.jose.pilae.model.Usuario;

@RestController
@RequestMapping(path = "/usuarios")
@CrossOrigin(origins="http://localhost:4200")
public class UsuarioControlador {

	@Autowired
	IUsuarioDominio usuarioDominio;
	
	@RequestMapping(value = "/saludar", method = RequestMethod.GET)
	public String saludar() {
		return "Qué tal!, ¿Cómo vamos?";
	}

	@RequestMapping(value = "/insertar", method = RequestMethod.POST)
	public boolean insertarUsuario(@RequestBody Usuario usuario) {
		return usuarioDominio.insertarUsuario(usuario);
	}

	@RequestMapping(value = "/consultar_por_identificacion", method = RequestMethod.GET)
	public Usuario consultarUsuario(@RequestBody String identificacion) {
		return usuarioDominio.consultarUsuarioPorIdentificacion(identificacion);
	}

	@RequestMapping(value = "/consultar_por_nombre", method = RequestMethod.GET)
	public List<Usuario> consultarUsuarioPorNombre(@RequestBody String nombre) {
		return usuarioDominio.consultarUsuarioPorNombre(nombre);
	}

	@RequestMapping(value = "/actualizar", method = RequestMethod.POST)
	public boolean actualizarUsuario(@RequestBody Usuario usuario) {
		return usuarioDominio.actualizarUsuario(usuario);
	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.POST)
	public boolean eliminarUsuario(@RequestBody Usuario usuario) {
		return usuarioDominio.eliminarUsuario(usuario);
	}

}
