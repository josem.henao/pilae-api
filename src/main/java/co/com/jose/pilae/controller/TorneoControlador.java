package co.com.jose.pilae.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.jose.pilae.dominio.usuario.ITorneoDominio;
import co.com.jose.pilae.model.Torneo;

@RestController
@RequestMapping(path = "/torneos")
public class TorneoControlador {

	@Autowired
	ITorneoDominio torneoDominio;


	@RequestMapping(value = "/saludar", method = RequestMethod.GET)
	public String saludar(@RequestBody Torneo torneo) {
		System.out.println("Saludando a Pilae");
		return "Hola Pilae";
	}

	@RequestMapping(value = "/insertar", method = RequestMethod.POST)
	public boolean insertarTorneo(@RequestBody Torneo torneo) {
		return torneoDominio.insertarTorneo(torneo);
	}

	@RequestMapping(value = "/consultarPorId", method = RequestMethod.GET)
	public Torneo consultarTorneo(@RequestBody int id_torneo) {
		return torneoDominio.consultarTorneoPorId(id_torneo);
	}

	@RequestMapping(value = "/consultarPorNombre", method = RequestMethod.GET)
	public List<Torneo> consultarTorneoPorNombre(@RequestBody String nombre) {
		return torneoDominio.consultarTorneoPorNombre(nombre);
	}

	@RequestMapping(value = "/actualizar", method = RequestMethod.PUT)
	public boolean actualizarTorneo(@RequestBody Torneo torneo) {
		return torneoDominio.actualizarTorneo(torneo);
	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.DELETE)
	public boolean eliminarTorneo(@RequestBody Torneo torneo) {
		return torneoDominio.eliminarTorneo(torneo);
	}

	@RequestMapping(value = "/eliminarPorId", method = RequestMethod.DELETE)
	public boolean eliminarTorneoPorId(@RequestBody int id) {
		return torneoDominio.eliminarTorneoPorIdentificacion(id);
	}
}
