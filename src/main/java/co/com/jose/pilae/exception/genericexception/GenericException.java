package co.com.jose.pilae.exception.genericexception;

public class GenericException extends Exception {

	private static final long serialVersionUID = 7767683557082716724L;
	private final String  mensaje;

	public GenericException(String mensaje, Exception e) {
		super(e.getMessage(), e);
		this.mensaje = mensaje;
	}

	public GenericException(String mensaje) {
		super(mensaje);
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

}
