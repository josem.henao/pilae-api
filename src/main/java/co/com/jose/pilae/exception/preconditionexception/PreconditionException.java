package co.com.jose.pilae.exception.preconditionexception;

public class PreconditionException extends Exception {

	private static final long serialVersionUID = 7767683557082716724L;
	private final String mensaje;

	public PreconditionException(String mensaje, Exception e) {
		super(e.getMessage(), e);
		this.mensaje = mensaje;
	}

	public PreconditionException(String mensaje) {
		super(mensaje);
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}
}
