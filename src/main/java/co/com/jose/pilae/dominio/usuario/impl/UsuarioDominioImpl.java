package co.com.jose.pilae.dominio.usuario.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.jose.pilae.dominio.usuario.IUsuarioDominio;
import co.com.jose.pilae.model.Usuario;
import co.com.jose.pilae.repository.IUsuarioRepository;

@Service
public class UsuarioDominioImpl implements IUsuarioDominio {

	@Autowired
	IUsuarioRepository usuarioRepositorio;

	@Override
	public boolean insertarUsuario(Usuario usuario) {
		co.com.jose.pilae.entity.Usuario uInsertado = usuarioRepositorio.save(usuario.asUsuarioEntidad());
		return true;
//		return uInsertado != null ? true : false;
	}

	@Override
	public Usuario consultarUsuarioPorIdentificacion(String identificacion) {
		co.com.jose.pilae.entity.Usuario uConsultado = usuarioRepositorio.consultarPorIdentificacion(identificacion);
		return uConsultado != null ? uConsultado.asUsuarioModelo() : null;
	}

	@Override
	public List<Usuario> consultarUsuarioPorNombre(String nombre) {
		List<co.com.jose.pilae.entity.Usuario> uConsultados = usuarioRepositorio.consultarPorNombre(nombre);
		return uConsultados != null ? usuariosEntidadAsUsuariosModelo(uConsultados) : null;
	}

	@Override
	public boolean actualizarUsuario(Usuario usuario) {
		boolean actualizado = true;
		co.com.jose.pilae.entity.Usuario usuarioActualizado = usuario.asUsuarioEntidad();
		try {
			this.usuarioRepositorio.flush();
		} catch (Exception e) {
			actualizado = false;
		}
		return actualizado;
	}

	@Override
	public boolean eliminarUsuario(Usuario usuario) {
		usuarioRepositorio.delete(usuario.asUsuarioEntidad());
		return false;
	}

	private List<Usuario> usuariosEntidadAsUsuariosModelo(List<co.com.jose.pilae.entity.Usuario> usuariosEntidad) {
		List<Usuario> usuariosModelo = null;
		for (co.com.jose.pilae.entity.Usuario u : usuariosEntidad) {
			usuariosModelo.add(u.asUsuarioModelo());
		}
		return usuariosModelo;
	}
}
