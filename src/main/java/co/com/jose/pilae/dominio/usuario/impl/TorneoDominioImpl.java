package co.com.jose.pilae.dominio.usuario.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.jose.pilae.dominio.usuario.ITorneoDominio;
import co.com.jose.pilae.model.Torneo;
import co.com.jose.pilae.repository.ITorneoRepository;

@Service
public class TorneoDominioImpl implements ITorneoDominio {

	@Autowired
	ITorneoRepository torneoRepositorio;

	@Override
	public boolean insertarTorneo(Torneo torneo) {
		co.com.jose.pilae.entity.Torneo tInsertado = torneoRepositorio.save(torneo.asTorneoEntidad());
		return tInsertado != null ? true : false;
	}

	@Override
	public Torneo consultarTorneoPorId(int id_torneo) {
		Torneo tConsultado = torneoRepositorio.findOne(id_torneo).asTorneoModelo();
		return tConsultado;
	}

	@Override
	public List<Torneo> consultarTorneoPorNombre(String nombre) {
		List<co.com.jose.pilae.entity.Torneo> torneos = torneoRepositorio.consultarPorNombre(nombre);
		return torneosAsListModel(torneos);
	}

	@Override
	public boolean actualizarTorneo(Torneo torneo) {
		boolean actualizado = true;
		co.com.jose.pilae.entity.Torneo tActualizado = torneo.asTorneoEntidad();
		try {
			this.torneoRepositorio.flush();
		} catch (Exception e) {
			actualizado = false;
		}
		return actualizado;
	}

	@Override
	public boolean eliminarTorneo(Torneo torneo) {
		this.torneoRepositorio.delete(torneo);
		return false;
	}

	@Override
	public boolean eliminarTorneoPorIdentificacion(int id_torneo) {
		boolean eliminado = true;
		try {
			this.torneoRepositorio.delete(id_torneo);
		} catch (Exception e) {
			eliminado = false;
		}
		return false;
	}
	
	private List<Torneo> torneosAsListModel(List<co.com.jose.pilae.entity.Torneo> torneos) {
		List<Torneo> torneosModel = null;
		for (co.com.jose.pilae.entity.Torneo t : torneos) {
			torneosModel.add(t.asTorneoModelo());
		}
		return torneosModel;
	}
}
