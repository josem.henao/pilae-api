package co.com.jose.pilae.dominio.usuario;

import java.util.List;

import co.com.jose.pilae.model.Usuario;

public interface IUsuarioDominio {

	boolean insertarUsuario(Usuario usuario);

	Usuario consultarUsuarioPorIdentificacion(String identificacion);

	List<Usuario> consultarUsuarioPorNombre(String nombre);

	boolean actualizarUsuario(Usuario usuario);

	boolean eliminarUsuario(Usuario usuario);

}
