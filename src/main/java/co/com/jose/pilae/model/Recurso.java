package co.com.jose.pilae.model;

import java.io.Serializable;

public class Recurso implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id_recurso;

	private String recurso;

	private String descripcion;

	private char activo;

	public co.com.jose.pilae.entity.Recurso asRecursoEntidad() {
		co.com.jose.pilae.entity.Recurso recurso = null;
		recurso.setId_recurso(this.id_recurso);
		recurso.setRecurso(this.recurso);
		recurso.setDescripcion(this.descripcion);
		recurso.setActivo(this.activo);
		return recurso;
	}

	public int getId_recurso() {
		return id_recurso;
	}

	public void setId_recurso(int id_recurso) {
		this.id_recurso = id_recurso;
	}

	public String getRecurso() {
		return recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public char getActivo() {
		return activo;
	}

	public void setActivo(char activo) {
		this.activo = activo;
	}
}
