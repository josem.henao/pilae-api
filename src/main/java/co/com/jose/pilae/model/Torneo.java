package co.com.jose.pilae.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

public class Torneo implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id_torneo;

	private String nombre;

	private String descripcion;

	private Calendar fecha_inicio;

	private Calendar fecha_fin;

	private String imagen;

	private String premiacion;

	private String desc_premiacion;

	private Genero genero;

	private Modalidad modalidad;

	private Categoria categoria;

	private List<Equipo> equipos;

	private List<Usuario> administradores;

	public co.com.jose.pilae.entity.Torneo asTorneoEntidad() {
		co.com.jose.pilae.entity.Torneo torneo = null;
		torneo.setId_torneo(this.id_torneo);
		torneo.setNombre(this.nombre);
		torneo.setDescripcion(this.descripcion);
		torneo.setFecha_inicio(this.fecha_fin);
		torneo.setFecha_fin(this.fecha_fin);
		torneo.setImagen(this.imagen);
		torneo.setPremiacion(this.premiacion);
		torneo.setDesc_premiacion(this.desc_premiacion);
		torneo.setCategoria(this.categoria.asCategoriaEntidad());
		torneo.setModalidad(this.modalidad.asModalidadEntidad());
		torneo.setGenero(this.genero.asGeneroEntidad());
		torneo.setAdministradores(getAdministradoresAsEntidades(this.administradores));
		torneo.setEquipos(getEquiposAsEntidades(this.equipos));
		return torneo;
	}

	public List<co.com.jose.pilae.entity.Usuario> getAdministradoresAsEntidades(List<Usuario> administradoresModelos) {
		List<co.com.jose.pilae.entity.Usuario> administradoresEntidades = null;
		for (Usuario a : administradoresModelos) {
			administradoresEntidades.add(a.asUsuarioEntidad());
		}
		return administradoresEntidades;
	}

	public List<co.com.jose.pilae.entity.Equipo> getEquiposAsEntidades(List<Equipo> equiposModelos) {
		List<co.com.jose.pilae.entity.Equipo> equiposEntidades = null;
		for (Equipo e : equiposModelos) {
			equiposEntidades.add(e.asEquipoModelo());
		}
		return equiposEntidades;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public int getId_torneo() {
		return id_torneo;
	}

	public void setId_torneo(int id_torneo) {
		this.id_torneo = id_torneo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Calendar getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Calendar fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public Calendar getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Calendar fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getPremiacion() {
		return premiacion;
	}

	public void setPremiacion(String premiacion) {
		this.premiacion = premiacion;
	}

	public String getDesc_premiacion() {
		return desc_premiacion;
	}

	public void setDesc_premiacion(String desc_premiacion) {
		this.desc_premiacion = desc_premiacion;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Modalidad getModalidad() {
		return modalidad;
	}

	public void setModalidad(Modalidad modalidad) {
		this.modalidad = modalidad;
	}

	public List<Equipo> getEquipos() {
		return equipos;
	}

	public void setEquipos(List<Equipo> equipos) {
		this.equipos = equipos;
	}

	public List<Usuario> getAdministradores() {
		return administradores;
	}

	public void setAdministradores(List<Usuario> administradores) {
		this.administradores = administradores;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
