package co.com.jose.pilae.model;

import java.io.Serializable;

public class Modalidad implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id_modalidad;

	private String modalidad;

	private String descripcion;

	public co.com.jose.pilae.entity.Modalidad asModalidadEntidad() {
		co.com.jose.pilae.entity.Modalidad modalidad = null;
		modalidad.setId_modalidad(this.id_modalidad);
		modalidad.setId_modalidad(this.id_modalidad);
		modalidad.setDescripcion(this.descripcion);
		return modalidad;
	}

	public int getId_modalidad() {
		return id_modalidad;
	}

	public void setId_modalidad(int id_modalidad) {
		this.id_modalidad = id_modalidad;
	}

	public String getModalidad() {
		return modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
