package co.com.jose.pilae.model;

import java.io.Serializable;
import java.util.List;

public class Equipo implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id_equipo;

	private int nombre;

	private String imagen;

	private Modalidad modalidad;

	public co.com.jose.pilae.entity.Equipo asEquipoModelo() {
		co.com.jose.pilae.entity.Equipo equipo = null;
		equipo.setId_equipo(this.id_equipo);
		equipo.setNombre(this.nombre);
		equipo.setImagen(this.imagen);
		equipo.setModalidad(this.modalidad.asModalidadEntidad());
		equipo.setCategoria(this.categoria.asCategoriaEntidad());
		equipo.setGenero(this.genero.asGeneroEntidad());
		return equipo;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	private Categoria categoria;

	private Genero genero;

	private List<Usuario> jugadores;

	public int getId_equipo() {
		return id_equipo;
	}

	public void setId_equipo(int id_equipo) {
		this.id_equipo = id_equipo;
	}

	public int getNombre() {
		return nombre;
	}

	public void setNombre(int nombre) {
		this.nombre = nombre;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public Modalidad getModalidad() {
		return modalidad;
	}

	public void setModalidad(Modalidad modalidad) {
		this.modalidad = modalidad;
	}

	public List<Usuario> getJugadores() {
		return jugadores;
	}

	public void setJugadores(List<Usuario> jugadores) {
		this.jugadores = jugadores;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
