package co.com.jose.pilae.model;

import java.io.Serializable;
import java.util.List;

public class Rol implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id_rol;

	private String rol;

	private String descripcion;

	private char activo;

	private List<Recurso> recursos;

	public co.com.jose.pilae.entity.Rol asRolEntidad() {
		co.com.jose.pilae.entity.Rol rol = null;
		rol.setId_rol(this.id_rol);
		rol.setRol(this.rol);
		rol.setDescripcion(this.descripcion);
		rol.setActivo(this.activo);
		return rol;
	}

	public int getId_rol() {
		return id_rol;
	}

	public void setId_rol(int id_rol) {
		this.id_rol = id_rol;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public char getActivo() {
		return activo;
	}

	public void setActivo(char activo) {
		this.activo = activo;
	}

	public List<Recurso> getRecursos() {
		return recursos;
	}

	public void setRecursos(List<Recurso> recursos) {
		this.recursos = recursos;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
