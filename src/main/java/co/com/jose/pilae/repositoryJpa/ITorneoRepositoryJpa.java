//package co.com.jose.pilae.repositoryJpa;
//
//import java.io.Serializable;
//import java.util.List;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//
//import co.com.jose.pilae.entity.Torneo;
//
//public interface ITorneoRepositoryJpa extends JpaRepository<Torneo, Serializable> {
//
//	@Query("select t from Torneo t where t.id_torneo = :id_torneo")
//	Torneo consultarPorId(@Param("id_torneo") int id_torneo);
//
//	@Query("select t from Torneo t where t.nombre = :nombre")
//	List<Torneo> consultarPorNombre(@Param("nombre") String nombre);
//
//}
