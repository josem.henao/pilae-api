//package co.com.jose.pilae.repositoryJpa;
//
//import java.io.Serializable;
//import java.util.List;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//
//import co.com.jose.pilae.entity.Usuario;
//
//public interface IUsuarioRepositoryJpa extends JpaRepository<Usuario, Serializable> {
//	@Query("select u from Usuario u where u.identificacion = :identificacion")
//	Usuario consultarPorIdentificacion(@Param("identificacion") String identificacion);
//
//	@Query("select u from Usuario u where u.nombre = :nombre")
//	List<Usuario> consultarPorNombre(@Param("nombre") String nombre);
//
//}
