package co.com.jose.pilae.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Torneo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_torneo;

	private String nombre;

	private String descripcion;

	private Calendar fecha_inicio;

	private Calendar fecha_fin;

	private String imagen;

	private String premiacion;

	private String desc_premiacion;

	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_genero", nullable = false)
	private Genero genero;

	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_modalidad", nullable = false)
	private Modalidad modalidad;

	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_categoria", nullable = false)
	private Categoria categoria;

	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_equipo", nullable = false)
	private List<Equipo> equipos;

	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario", nullable = false)
	private List<Usuario> administradores;

	public co.com.jose.pilae.model.Torneo asTorneoModelo() {
		co.com.jose.pilae.model.Torneo torneo = null;

		torneo.setId_torneo(this.id_torneo);
		torneo.setNombre(this.nombre);
		torneo.setDescripcion(this.descripcion);
		torneo.setFecha_inicio(this.fecha_fin);
		torneo.setFecha_fin(this.fecha_fin);
		torneo.setImagen(this.imagen);
		torneo.setPremiacion(this.premiacion);
		torneo.setDesc_premiacion(this.desc_premiacion);
		torneo.setCategoria(this.categoria.asCategoriaModelo());
		torneo.setModalidad(this.modalidad.asModalidadModelo());
		torneo.setGenero(this.genero.asGeneroModelo());
		torneo.setAdministradores(getAdministradoresAsModelos(this.administradores));
		torneo.setEquipos(getEquiposAsModelos(this.equipos));

		return torneo;
	}

	public List<co.com.jose.pilae.model.Usuario> getAdministradoresAsModelos(List<Usuario> administradoresEntidad) {
		List<co.com.jose.pilae.model.Usuario> administradoresModelo = null;
		for (Usuario a : administradoresEntidad) {
			administradoresModelo.add(a.asUsuarioModelo());
		}
		return administradoresModelo;
	}

	public List<co.com.jose.pilae.model.Equipo> getEquiposAsModelos(List<Equipo> equiposEntidad) {
		List<co.com.jose.pilae.model.Equipo> equiposModelo = null;
		for (Equipo e : equiposEntidad) {
			equiposModelo.add(e.asEquipoModelo());
		}
		return equiposModelo;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public int getId_torneo() {
		return id_torneo;
	}

	public void setId_torneo(int id_torneo) {
		this.id_torneo = id_torneo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Calendar getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Calendar fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public Calendar getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Calendar fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getPremiacion() {
		return premiacion;
	}

	public void setPremiacion(String premiacion) {
		this.premiacion = premiacion;
	}

	public String getDesc_premiacion() {
		return desc_premiacion;
	}

	public void setDesc_premiacion(String desc_premiacion) {
		this.desc_premiacion = desc_premiacion;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Modalidad getModalidad() {
		return modalidad;
	}

	public void setModalidad(Modalidad modalidad) {
		this.modalidad = modalidad;
	}

	public List<Equipo> getEquipos() {
		return equipos;
	}

	public void setEquipos(List<Equipo> equipos) {
		this.equipos = equipos;
	}

	public List<Usuario> getAdministradores() {
		return administradores;
	}

	public void setAdministradores(List<Usuario> administradores) {
		this.administradores = administradores;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
