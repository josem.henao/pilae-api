package co.com.jose.pilae.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Categoria implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_categoria;

	private String categoria;

	private String descripcion;

	public co.com.jose.pilae.model.Categoria asCategoriaModelo() {
		co.com.jose.pilae.model.Categoria categoria = null;
		categoria.setId_categoria(this.id_categoria);
		categoria.setCategoria(this.categoria);
		categoria.setDescripcion(this.descripcion);
		return categoria;
	}

	public int getId_categoria() {
		return id_categoria;
	}

	public void setId_categoria(int id_categoria) {
		this.id_categoria = id_categoria;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
